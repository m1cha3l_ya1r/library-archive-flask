import datetime
import json
import flask
from flask import Flask, redirect, session, request, abort, render_template, send_from_directory, g
import os
from google_auth_oauthlib.flow import Flow
from google.cloud import storage
from models.server import *
import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore
from flask_bootstrap import Bootstrap
from utilities.tools import allowed_files, csv_to_dictionary, validate_text, refactor_dict, md2html, \
    id_info, login_is_required


server = Server()
host = server.host
port = server.port
base_url = host if port is None else f"{host}:{port}"

app = Flask(__name__)
bootstrap = Bootstrap(app)
app.secret_key = os.environ.get("SECRET_KEY")
os.environ["OAUTHLIB_INSECURE_TRANSPORT"] = "1"

###########################################################################
# Project OAuth 2.1 Client ID
CLIENT_SECRETS = {}
with open(Secrets.client_secrets_file, 'r') as file:
    d = dict(json.load(file))
    for k in d['web'].keys():
        CLIENT_SECRETS[k] = d['web'][k]
CLIENT_ID = CLIENT_SECRETS['client_id']

# Storage Service Account
APPLICATION_NAME = "library-data-archive-demo-UoH"
APP_CLIENT = storage.Client.from_service_account_json(json_credentials_path=Secrets.PROJECT_CREDENTIALS)

scopes = ["openid",
          "https://www.googleapis.com/auth/userinfo.profile",
          "https://www.googleapis.com/auth/userinfo.email"
          ]
flow = Flow.from_client_secrets_file(
    client_secrets_file=Secrets.client_secrets_file,
    scopes=scopes,
    redirect_uri=f"{base_url}/auth",
)

# Firestore Service Account
cred = credentials.Certificate(Secrets.FB_ADMIN)
firebase_admin.initialize_app(cred)
db = firestore.client()
###########################################################################

# Global variables
datetime_format = "%Y-%m-%d, %H:%M"
allowed_text = "^[a-zA-Z0-9,.;:!? \-\(\)\r\n]+$"
negate_allowed_text = "[^a-zA-Z0-9,.;!? \-\(\)\r\n]"
allowed_file_extensions = {'csv', 'json'}



@app.route('/favicon.ico')
def favicon():
    return send_from_directory(os.path.join(app.root_path, 'static'),
                               'favicon.ico', mimetype='image/vdn.microsoft.icon')


@app.route('/login')
def login():
    authorization_url, state = flow.authorization_url(prompt="consent")
    session["state"] = state
    return redirect(authorization_url)


@app.route('/auth')
def auth():
    session["code"] = request.args["code"]
    tokens = flow.fetch_token(code=session["code"])
    if "state" not in session and session["state"] != request.args["state"]:
        abort(500)
    for key in tokens.keys():
        session[key] = tokens[key]
    id_info(flow, CLIENT_ID)
    return redirect("/buckets")


@app.route("/logout")
def logout():
    session.clear()
    return redirect("/")


@app.route("/")
@app.route("/home")
@login_is_required
def home(logged_in):
    return render_template(
        "home.html",
        logged_in=logged_in,
        name=session['name'] if 'name' in session else None,
        home_md=md2html("static/pages/home.md")
    )


@app.route("/about")
@login_is_required
def about(logged_in):
    return render_template(
        "about.html",
        logged_in=logged_in,
        name=session['name'] if 'name' in session else None
    )


@app.route("/buckets", methods=["GET", "POST"])
@login_is_required
def buckets(logged_in):
    if not logged_in:
        return redirect("/")
    if request.method == "GET":
        buckets_iam_policy = APP_CLIENT.list_buckets()
        buckets_list = []
        for bucket in buckets_iam_policy:
            policy = bucket.get_iam_policy(requested_policy_version=3)
            for binding in policy.bindings:
                if f"user:{session['email']}" in binding['members']:
                    buckets_list.append(bucket.name)
                    break
        g.buckets_list = buckets_list
        return render_template(
            "buckets.html",
            logged_in=logged_in,
            name=session['name'] if 'name' in session else None,
            buckets_list=buckets_list,
        )
    if request.method == "POST":
        session["bucket_name"] = request.form.get("bucket_name")
        return redirect("/buckets/metadata_form")


@app.route("/buckets/metadata_form", methods=["GET", "POST"])
@login_is_required
def metadata_form(logged_in):
    if not logged_in:
        return redirect("/")
    bucket = APP_CLIENT.bucket(bucket_name=session["bucket_name"])
    policy = bucket.get_iam_policy(requested_policy_version=3)
    exp_timestamp = None
    for binding in policy.bindings:
        try:
            if f"user:{session['email']}" in binding['members']:
                binding_condition_timestamp = binding['condition']['expression'].split("\"")[-2][:-1]
                exp_timestamp = datetime.datetime.fromisoformat(binding_condition_timestamp)
        except KeyError as key_err:
            flask.flash(f"The permissions of the selected bucket have no expiration time. missing {key_err}.", "warning")
            exp_timestamp = None
    if request.method == "GET":
        doc = db.collection(u"buckets_metadata").document(session["bucket_name"]).get()
        if not doc.exists:
            init = {
                "bucket": {
                    "name": session["bucket_name"]
                },
                "author": {
                    "name": session["name"],
                    "prefix": "",
                    "role": "",
                },
                "org": {
                    "name": "",
                    "faculty": "",
                    "unit_lab": ""
                },
                "db": {
                    "name": "",
                    "description": "",
                    "metadata": {}
                },
                "submitted": False,
                "public": False
            }
            db.collection(u"buckets_metadata").document(session["bucket_name"]).set(init)
            db.collection(u"buckets_metadata").document(session["bucket_name"]). \
                collection("allowed_users").document(session["sub"]).set(
                {
                    "expires": datetime.datetime.now() + datetime.timedelta(days=10)
                })
        doc = db.collection(u"buckets_metadata").document(session["bucket_name"]).get()
        doc_dict = dict(sorted(doc.to_dict().items()))
        update_time = datetime.datetime.fromtimestamp(doc.update_time.timestamp()).strftime(datetime_format)
        return render_template("buckets.html",
                               logged_in=logged_in,
                               name=session['name'] if 'name' in session else None,
                               selected_bucket=bucket,
                               date_last_update=update_time,
                               exp_timestamp=exp_timestamp.strftime(datetime_format) if exp_timestamp is not None else "",
                               doc_dict=doc_dict,
                               allowed_text=allowed_text,
                               message_timestamp=datetime.datetime.now().strftime(datetime_format)
                               )
    if request.method == "POST":
        update = {}
        for key, value in request.form.to_dict().items():
            if key not in ["action", "form_file"] and validate_text(allowed_text, value):
                update[key] = value
        if update:
            update["public"] = True if update["public"] is "1" else False
            db.collection(u"buckets_metadata").document(session["bucket_name"]).update(update)
            flask.flash("Updated", "info")
            return redirect("/buckets/metadata_form")


@app.route("/buckets/upload_metadata_file", methods=["GET", "POST"])
@login_is_required
def upload_metadata_file(logged_in):
    if not logged_in:
        return redirect("/")
    update = {}
    bucket = APP_CLIENT.bucket(bucket_name=session["bucket_name"])
    policy = bucket.get_iam_policy(requested_policy_version=3)
    members = []
    for binding in policy.bindings:
        members.extend(binding['members'])
    if f"user:{session['email']}" not in members:
        flask.flash("redirected back to /, you do not have permission to upload metadata file", "warning")
        redirect("/")
    try:
        if "from_file" in request.files:
            request_file = request.files["from_file"]
            if request_file.filename == '':
                flask.flash("No file selected", "warning")
                return redirect(request.url)
            if request_file and allowed_files(request_file.filename, allowed_file_extensions):
                if "csv" in request_file.filename:
                    str_file = request_file.read().decode("utf-8")
                    dict_str_file = csv_to_dictionary(str_file)
                    update["db.metadata"] = refactor_dict(negate_allowed_text, dict_str_file)

                if "json" in request_file.filename:
                    str_file = request_file.read().decode("utf-8")
                    dict_str_file = json.loads(str_file)
                    update["db.metadata"] = refactor_dict(negate_allowed_text, dict_str_file)
            else:
                flask.flash("file type is not supported", "error")
                redirect("/buckets/metadata_form")
    except ValueError as err:
        flask.flash(str(err), "error")
        return redirect("/buckets/metadata_form")
    if update:
        db.collection(u"buckets_metadata").document(session["bucket_name"]).update(update)
        flask.flash("Updated", "info")
    return redirect("/buckets/metadata_form")


@app.route("/buckets/<bucket_name>", methods=["GET"])
@login_is_required
def bucket_info(logged_in, bucket_name):
    if not logged_in:
        return redirect("/")
    if request.method == "GET":
        bucket = APP_CLIENT.bucket(bucket_name=bucket_name)
        blobs_names = [blob.name for blob in bucket.list_blobs()]
        doc = db.collection(u"buckets_metadata").document(bucket_name).get()
        update_time = datetime.datetime.fromtimestamp(doc.update_time.timestamp()).strftime(datetime_format)
        doc_dict = doc.to_dict()
    else:
        flask.flash("GET request is the only allowed", "warning")
        return redirect("/")
    return render_template(
        "buckets.html",
        logged_in=logged_in,
        name=session['name'] if 'name' in session else None,
        blobs_names=blobs_names,
        doc_dict=doc_dict,
        allowed_text=allowed_text,
        date_last_update=update_time,
    )


@app.route("/documentation")
@login_is_required
def docs(logged_in):
    docs_pages = {"overview": ["purpose", "structure"], "how_to": ["prerequisites", "data_upload", "examples"]}
    docs_pages_md = {}
    for p in docs_pages:
        docs_pages_md[p] = md2html(f"static/docs/{p}.md")
    return render_template(
        "documentation.html",
        logged_in=logged_in,
        name=session['name'] if 'name' in session else None,
        docs_pages=docs_pages,
        docs_pages_md=docs_pages_md
    )


@app.route("/search", methods=["GET"])
@login_is_required
def search(logged_in):
    if not logged_in:
        return redirect("/")
    all_buckets = db.collection(u"buckets_metadata").stream()
    all_buckets_dict = {}
    for bucket in all_buckets:
        all_buckets_dict[bucket.id] = bucket.to_dict()
    return render_template(
        "search.html",
        logged_in=logged_in,
        name=session['name'] if 'name' in session else None,
        docs=all_buckets_dict,
        base_url=base_url
    )


if __name__ == '__main__':
    app.run(host=host, port=port, debug=True, threaded=True)
