### <darkred id="overview">1. <u>Overview</u></darkred>
#### <darkred>1.1. Purpose</darkred>
Our app allow any permitted user, to get a dedicated bucket to archive a research data. <br/>
Archived data will be findable around the world, accessible to any end user and reusable.<br/>

#### <darkred>1.2. Structure</darkred>

- Logging in to the system let you access to the dedicated bucket.
- There you may upload your data (directly from Google Console).
- You have to add general information about the author, organization and the data itself.
- To make the data findable, upload a metadata file in the designated area in the form.
- See How To section for further details.

<style>
    darkred { color: darkred; }
</style>