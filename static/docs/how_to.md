### <darkred id="how_to">2. <u>How To?</u></darkred>
#### <darkred id="prerequisites">2.1. Prerequisites</darkred>
<p><darkred>2.1.1. There are 2 optional usages to this application:</darkred></p>

- Search for data
- Archive data

<p>This <u>How to</u> section will focus the Archive of data.</p>

<p><darkred>2.1.2. Make sure you already followed the following steps:</darkred></p>

- Contact the admin to get a bucket
- Request was approved and you have received a confirmation and a bucket link/name

#### <darkred id="data_upload">2.2. Data upload:</darkred>

- If your data contains more than 50GiB, zip the data into 50GiB parts
- Go to the bucket console
- Upload the zip files to the bucket root folder,
  do not create folders and sub folders, the whole folders tree should be zipped.

<p><darkred>2.2.1. Fill the metadata form:</darkred></p>

- Go to the app [webpage](https://library-storage-poc.ey.r.appspot.com) and login
- Go to Buckets
- Select your bucket (by name)
- Fill the form

<p><darkred>2.2.2. Create a metadata file:</darkred></p>

- JSON: this is the simplest option, follow the rules of creating a JSON file with your favorite IDE
- CSV: currently supports only 2 columns - KEY and VALUE (titles are not required). 
  Although the KEY can represent a nested structure seperated by ".", 
  and the VALUE a short explanation describing the meaning of that KEY.
- See example below

#### <darkred id="examples">2.3. Metadata file example:</darkred>
The table represents a data structure, for a metadata file:


| Key Level 1    | Key Level 2 | Key Level 3 | Value Meaning                            |
|----------------|-------------|-------------|------------------------------------------|
| Book           | Name        |             | a book name                              |
| Author         | Name        | First_Name  | author first name                        |
| Author         | Name        | Last_Name   | author last name                         |
| Author         | Phone       | Home        | author home phone number                 |
| Author         | Phone       | Work        | author work phone number                 |
| Author         | Address     |             | author address                           |
| Author         | Email       |             | author email                             |
| List of Genres |             |             | list of books genres related to the book |

<br/>
<darkred>2.3.1. A JSON format:</darkred>

``` json
{
  "Book" : {
  "Name": "a book name"
  },
  "Author": {
    "Name": 
      {
      "First_Name": "author first name",
      "Last_Name": "author last name"
    },
    "Phone": {
      "Home": "author phone at home",
      "Work": "author phone at work"
    },
    "Address": "author address",
    "Email": "author email"
  },
  "List of Books": "list of books"
}
```
<br/>
<darkred>2.3.2. A nested CSV format (the titles row is not required) using Excel: </darkred>

| Level1.Level2.Level3   | Value Meaning                            |
|------------------------|------------------------------------------|
| Book.Name              | a book name                              |
| Author.Name.First_Name | author first name                        |
| Author.Name.Last_Name  | author last name                         |
| Author.Phone.Home      | author home phone number                 |
| Author.Phone.Work      | author work phone number                 |
| Author.Address         | author address                           |
| Author.Email           | author email                             |
| List of Genres         | list of books genres related to the book |


<br/>
<darkred>2.3.3. A nested CSV format using IDE: </darkred>

```csv
Book.Name, a book name
Author.Name.First_Name, author first name
Author.Name.Last_Name, author last name
Author.Phone.Home, author home phone number
Author.Phone.Work, author work phone number
Author.Address, author address
Author.Email, author email
List of Genres, list of books genres related to the book
```

<style>
darkred {color: darkred;}
black {color: black}
table, th, td { 
  padding: 5px;
  border: 1px solid darkred;
  color: black;
}
th {
 color: darkred;
}
pre {
  width: 50%;
  background: black;
  padding: 5px;
}

</style>