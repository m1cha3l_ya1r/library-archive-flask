# library-archive-flask

## Prolog
This project main goal is to demonstrate a full system of UI-Storage-noSQL database, 
to store static data post research publication.

## Stack
- Google App Engine: Flask server (Python3, Jinja, HTML)
- Google Storage: bucket to store data
- Google Firestore: noSQL database, store metadata

## How to start
### Prerequisite:
- Project on Google cloud
- the stack above
- create Python3 virtual environment
- run:
```
pip3 install -r requirements.txt
```
### Build requirements:
- create Service Account credential to each part of the stack
- create a project client ID (OAuth 2.1)
- save the files and link them to the variables at the beginning of main.py
- create **models** folder and in it create server.py with a Server class containing the server host address
- On Google Cloud -> APIs & Services -> Credentials -> OAuth 2.1 Client ID -> {the client ID you have created}: 
  - add {host URI} to the Authorized JavaScript origin
  - add {host URI}/auth and {host URL}/buckets to Authorized redirect URIs
  - Save
