import pytest
from tools import *
allowed_text = "^[a-zA-Z0-9,.;!? \-\(\)\r\n]+$"
negate_allowed_text = "[^a-zA-Z0-9,.;:!? \-\(\)\r\n]"


def test_validate_text():
    assert validate_text(allowed_text, "do (not) delete!?") == True
    assert validate_text(allowed_text, "del *") == False
    assert validate_text(allowed_text, "`do (somthing) $evil") == False
    assert validate_text(allowed_text,
                         """
                         I should add a scholar name, authors, abstract per each base on the DB.
                         Also DOI.
                         """
                         ) == True
    assert validate_text(allowed_text,
                         """
                         \I should add a scholar name, authors, abstract per each base on the DB.
                         Also DOI\.
                         """
                         ) == False
    assert validate_text(allowed_text, "rm *") == False


def test_refactor_dict():
    d = {
        "stuff": "or staff",
        "rm *": "or del *",
        "exec some function() {let js=Object()}": "not allowed <div> </div>",
        "author": {'name': '$`del` name of the data creator', 'prefix': 'author name prefix', 'role': 'author role or profession'},
        'db.description': 'Thing to add:\r\n1. Scholars list with abstract\r\n2. DOI'
    }
    assert refactor_dict(negate_allowed_text, d) == {
        "stuff": "or staff",
        "rm ": "or del ",
        "exec some function() let jsObject()": "not allowed div div",
        "author": {'name': 'del name of the data creator', 'prefix': 'author name prefix', 'role': 'author role or profession'},
        'db.description': 'Thing to add:\r\n1. Scholars list with abstract\r\n2. DOI'
    }


def test_unpack_dict():
    d1 = {
        "author.name.first": "Michael",
        "author.name.last": "Yair",
        "author.role": "Developer"
    }
    d2 = {
        "var1": "This is the most improtant variable",
        "var2": "this is a test",
        "var1.Test1": "",
        "var1.Test1.Test2": "TTTT",
        "var3": "today is the day",
        "var4": "",
        "var5": "toostoos"
    }
    assert unpack_dict(d1) == {
        "author":
            {
                "name": {
                    "first": "Michael",
                    "last": "Yair"
                },
                "role": "Developer"
            }
                             }
    with pytest.raises(ValueError):
        unpack_dict(d2)