from collections.abc import MutableMapping
from io import StringIO
import csv
import re
import markdown
import markdown.extensions.fenced_code
import markdown.extensions.codehilite
from pygments.formatters import HtmlFormatter
from functools import wraps
import google.auth.transport.requests
import requests
from google.oauth2 import id_token
import cachecontrol
from flask import session


# import nltk
# from nltk.corpus import stopwords

# nltk.download("stopwords")


def id_info(flow, client_id):
    request_session = requests.Session()
    cached_session = cachecontrol.CacheControl(request_session)
    token_request = google.auth.transport.requests.Request(
        session=cached_session,
    )
    try:
        verified = id_token.verify_oauth2_token(
            id_token=flow.credentials._id_token,
            request=token_request,
            audience=client_id
        )
    except:
        return None
    for key in verified.keys():
        session[key] = verified[key]
    return True


def check_session():
    if "id_token" in session:
        return True
    else:
        session.clear()
        return False


def login_is_required(function):
    @wraps(function)
    def wrapper(*args, **kwargs):
        logged_in = check_session()
        return function(logged_in, *args, **kwargs)

    return wrapper


def merge_dicts(a: dict, b: dict, path: list=[]) -> dict:
    """
    Merges nested dictionary b into nested dictionary a
    :param a: first input dictionary
    :param b: second input dictionary
    :param path: serves error handling text - the list will represent the path to the key causing the error
    :return: unified nested dictionaries
    """
    for key in b:
        if key in a:
            if isinstance(a[key], dict) and isinstance(b[key], dict):
                merge_dicts(a[key], b[key], path+[str(key)])
            elif a[key] == b[key]:
                pass
            else:
                raise ValueError(f"Your uploaded file have a conflict at the key named '{'.'.join(path+[str(key)])}', "
                                 f"it is probably a duplicated key or wrong nesting structure")
        else:
            a[key] = b[key]
    return a


def unpack_dict(d: dict) -> dict:
    """
    transform flat dictionary with key string '.' separated, representing nested form, to a nested dictionary
    :param d: flat dictionary
    :return: nested dictionary
    """
    new_d = {}
    try:
        for key, value in d.items():
            unpacked_key = key.rsplit('.', 1)
            temp_d = new_d.copy()
            if len(unpacked_key) > 1:
                new_d = merge_dicts(temp_d, unpack_dict({unpacked_key[0]: {unpacked_key[1]: value}}))
            else:
                new_d = merge_dicts(temp_d, {key: value})
    except ValueError as err:
        raise ValueError(err)
    return new_d


def allowed_files(file_name: str, allowed: set):
    return '.' in file_name and file_name.rsplit('.', 1)[1].lower() in allowed


def csv_to_dictionary(str_file: str):
    """
    transform a CSV stream to dictionary
    :param str_file: stream from uploaded file
    :return: dictionary
    """
    f = StringIO(str_file)
    delimiter = "\t" if "\t" in str_file else ","
    reader = csv.reader(f, delimiter=delimiter)
    # TODO: case the row is empty
    metadata = {}
    for row in reader:
        if isinstance(row, list) and len(row) == 2:
            metadata[row[0].replace(u"\ufeff", "")] = row[1]
    metadata = dict(sorted(metadata.items()))
    return unpack_dict(metadata)


def validate_text(allowed_characters: str, text: str):
    """
    Validate text received in request
    :param allowed_characters: the characters allowed on server
    :param text: text received from request
    :return: boolean
    """
    try:
        re.compile(allowed_characters)
        if re.fullmatch(allowed_characters, text, re.M):
            return True
        else:
            return False
    except re.error:
        return "invalid regex pattern"


def refactor_dict(pattern: str, dictionary: MutableMapping) -> MutableMapping:
    """
    Refactor text received from request in case there are unwonted characters
    :param pattern: allowed characters
    :param dictionary: dictionary received from request
    :return: refactored dictionary
    """
    re.compile(pattern)
    new_dictionary = {}
    for key, value in dictionary.items():
        new_value = ""
        if not validate_text(pattern, key):
            new_key = re.sub(pattern=pattern, repl='', string=key)
        else:
            new_key = key

        if isinstance(value, str) and not validate_text(pattern, value):
            new_value = re.sub(pattern=pattern, repl='', string=value)

        if isinstance(value, MutableMapping):
            new_value = refactor_dict(pattern, value)

        new_dictionary[new_key] = new_value
    return new_dictionary


def md2html(md_file):
    """
    transform md file to html
    :param md_file: string of file path
    :return: string in html format
    """
    formatter = HtmlFormatter(style="monokai", full=True, cssclass="codehilite")
    style_block = formatter.get_style_defs()
    style = f"<style>\n{style_block}\n</style"

    with open(file=md_file, mode="r", encoding="utf-8") as file:
        home_file = file.read()
    output = markdown.markdown(home_file, extensions=[
        'fenced_code',
        'codehilite',
        'tables',
        'extra'
    ]) + style
    return output
